<?php
/**
 * Denko DAO 0.1
 *
 * Container de funciones para DAOs
 *
 * @author Denko Developers Group <info at dokkogroup dot com dot ar>
 * @copyright Copyright (c) 2007-2009 Dokko Group.
 * @link http://www.dokkogroup.com.ar/
 * @version 1.0
 * @package Denko
 */
class DK_Dao{

    /**
     * @static
     * @access public
     * @var string prefijo para los DAOs en cache
     */
    public static $cache_prefix = 'DENKO_DAO_CACHE_';

    /**
     * @static
     * @access public
     * @var string alias de NULL
     */
    public static $null = '@NULL@';

    /**
     * Verifica si la variable es un ID de base de datos v�lido
     *
     * @param mixed $id ID que verificar
     * @static
     * @access public
     * @return boolean si la variable es un ID de base de datos v�lido
     */
    public static function isId($id){
        if(empty($id)){
            return false;
        }
        $id = trim($id);
        return (!empty($id) && is_numeric($id) && intval($id) == $id && $id > 0);
    }

    /**
     * Indica si un valor del DAO es vac�o o con contenido nulo
     *
     * @param mixed &$data valor de un campo del DAO
     * @static
     * @access public
     * @return boolean
     */
    public static function isEmpty(&$data){
        return ($data === null || $data === '' || $data === 'null');
    }

    /**
     * Obtiene un DAO, cacheando su resultado
     *
     * @param $table nombre de la tabla
     * @param $id id
     * @param $params par�metros extra
     * @static
     * @access public
     * @return DataObjects DAO
     */
    public static function getDao($table,$id,$params=array()){

        # Verifico que el nombre de la tabla sea v�lido
        $table = !empty($table) ? trim($table) : null;
        if(empty($table)){
            return null;
        }

        # Verifico que el ID sea v�lido
        $id = !empty($id) ? trim($id) : null;
        if(!self::isId($id)){
            return null;
        }

        # En caso que la info no est� cacheada, la cacheo
        $cachekey = self::$cache_prefix.strtoupper($table).'_'.$id;
        if(empty($GLOBALS[$cachekey])){

            # Obtengo la clave primaria
            $dao_pk = !empty($params['primary_key']) ? $params['primary_key'] : 'id_'.strtolower($table);

            # Verifico que la tabla exista en el esquema
            $dao = DB_DataObject::factory($table);
            if(get_class($dao) == 'DB_DataObject_Error'){
                return null;
            }

            # Verifico que el DAO exista
            if(!empty($params['selectAdd'])){
                $dao->selectAdd();
                $dao->selectAdd($params['selectAdd']);
            }
            $dao->$dao_pk = $id;
            if(!$dao->find(true)){
                $GLOBALS[$cachekey] = self::$null;
            }
            else{
                $GLOBALS[$cachekey] = $dao;
            }
        }

        # Retorno el DAO cacheado
        return (is_string($GLOBALS[$cachekey]) && $GLOBALS[$cachekey] == self::$null) ? null : $GLOBALS[$cachekey];
    }
}
################################################################################
?>