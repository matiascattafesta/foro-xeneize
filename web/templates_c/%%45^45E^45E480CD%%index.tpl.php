<?php /* Smarty version 2.6.28, created on 2014-05-08 13:52:25
         compiled from index.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'escape', 'index.tpl', 6, false),array('modifier', 'gravatar', 'index.tpl', 12, false),array('modifier', 'lower', 'index.tpl', 19, false),array('modifier', 'youtube', 'index.tpl', 23, false),array('modifier', 'emoticon', 'index.tpl', 23, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header.tpl", 'smarty_include_vars' => array('subtitulo' => 'Comentarios','titulo' => 'Foro Xeneixe','seccion' => 'index')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<?php $_from = $this->_tpl_vars['coment']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['com']):
?>
	<div class="cuadroComentario">
		<div class="cuadroComentDatos">
			<div> De: <?php echo ((is_array($_tmp=$this->_tpl_vars['com']['nombre'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'html') : smarty_modifier_escape($_tmp, 'html')); ?>
 </div>    
			<div><?php echo $this->_tpl_vars['com']['fecha']; ?>
</div>
			<div> 
				<?php if (( $this->_tpl_vars['com']['existeFoto'] == 1 )): ?>
					<img src="foto/<?php echo $this->_tpl_vars['com']['id_usuario']; ?>
-<?php echo $this->_tpl_vars['com']['nombre']; ?>
.jpg">
				<?php else: ?>
				  	<?php echo ((is_array($_tmp=$this->_tpl_vars['com']['mail'])) ? $this->_run_mod_handler('gravatar', true, $_tmp) : smarty_modifier_gravatar($_tmp)); ?>

				<?php endif; ?>
			</div>   
		</div>
		<div class="cuadroComentComent">
			<div class="titulo">
				<b>	<?php echo ((is_array($_tmp=$this->_tpl_vars['com']['titulo'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'html') : smarty_modifier_escape($_tmp, 'html')); ?>
 </b>
				<?php if (( $this->_tpl_vars['com']['nom'] == ((is_array($_tmp=$this->_tpl_vars['com']['nombre'])) ? $this->_run_mod_handler('lower', true, $_tmp) : smarty_modifier_lower($_tmp)) )): ?>
					<a href="borrarComentario.php?id=<?php echo $this->_tpl_vars['com']['id_comentario']; ?>
" class="borrar">X</a>
				<?php endif; ?>				
			</div>
			<div> <?php echo ((is_array($_tmp=((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['com']['comentario'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'html') : smarty_modifier_escape($_tmp, 'html')))) ? $this->_run_mod_handler('youtube', true, $_tmp) : smarty_modifier_youtube($_tmp)))) ? $this->_run_mod_handler('emoticon', true, $_tmp) : smarty_modifier_emoticon($_tmp)); ?>
 </div>
		</div>
	</div>
<?php endforeach; endif; unset($_from); ?>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "footer.tpl", 'smarty_include_vars' => array('seccion' => 'index')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>