<?php
require_once ('common.php');
$smarty = new Smarty;

$c=Denko::daoFactory('Comentario');

$arreglo_externo=array();

$c->find();

while ($c->fetch()){
	$arreglo_interno['titulo']=$c->titulo;

	$u=Denko::daoFactory('Usuario');
	$u->id_usuario=$c->id_usuario;
	$u->find(true);

	$arreglo_interno['nombre']=$u->nombre;
	$arreglo_interno['fecha']=$c->fecha;
	$arreglo_interno['comentario']=$c->comentario;
	$arreglo_interno['id_usuario']=$c->id_usuario;
	$arreglo_interno['existeFoto']=$u->existe_foto;
	$arreglo_interno['id_comentario']=$c->id_comentario;
	if (isset($_SESSION['usuario'])) {
		$arreglo_interno['nom']=$_SESSION['usuario'];
	}
	if (isset($_SESSION['id_usuario'])) {
		$arreglo_interno['id']=$_SESSION['id_usuario'];
	}
	$arreglo_externo[]=$arreglo_interno;
}

$smarty->assign("coment",$arreglo_externo);

$smarty->display('index.tpl');