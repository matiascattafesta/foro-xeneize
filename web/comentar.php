<?php
	require_once('common.php');
	$smarty = new Smarty;

	if (isset($_POST['comentario']) && $_POST['comentario'] != "") { 
		$c=Denko::daoFactory('Comentario');
		$c->titulo=$_POST['titulo'];
		$c->comentario=$_POST['comentario'];
		$c->fecha=Denko::curTimestamp();
		$u=Denko::daoFactory('Usuario');
		$u->nombre=$_SESSION['usuario'];
		$u->find(true);
		$c->id_usuario=$u->id_usuario;
		$c->insert();
		header('Location:index.php');
	} else {
			$smarty->display('formComentario.tpl');
		}
?>