<?php
	require_once('common.php');
	$smarty = new Smarty;

	
	if (isset($_POST['usuario']) && isset($_POST['password']) && isset($_POST['confirmar']) && isset($_POST['mail'])) {
			
		if (registrar($_POST['usuario'], $_POST['password'],$_POST['mail'])) {			
			$_SESSION['usuario'] = $_POST['usuario'];
			header('Location:index.php');
		} else {
			$smarty->assign('titulo',"Registro Fallido");
			$smarty->assign('subtitulo',"EL USUARIO YA EXISTE");
			$smarty->display('header.tpl');
			?>
			<a style="display:block ; text-align: center;"href="registro.php">REINTENTAR</a>
			<?php
		}

	} else {
		$smarty->display('formRegistro.tpl');
	}


	function registrar ($usuario,$password,$mail){
		$u=Denko::daoFactory('Usuario');
		$u->nombre=$usuario;
		if ($u->find()){
			return false;
		} else {		
			$u->password=$password;
			$u->mail=$mail;		
			$u->insert();
			$smarty->assign();
			return true;
		}
	}

