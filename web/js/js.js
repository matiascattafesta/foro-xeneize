function verificarLogin(){
	var usuario=document.getElementById('usuario').value;
	var clave=document.getElementById('password').value;	
  var error=document.getElementById('oculto');


   if (usuario=="" || clave=="") {
    error.style.display='block';
    error.innerHTML='No puede dejar campos sin completar.';
    return false;
  }
  return true;
  }

function verificarRegistro() {
  var usuario=document.getElementById('usuario').value;
  var clave=document.getElementById('password').value;
  var confirmacion=document.getElementById('confirmar').value;
  var error=document.getElementById('oculto');
  var mail=document.getElementById('mail');
  
  if (usuario=="" || clave=="" || confirmacion=="" || mail=="") {
    error.style.display='block';
    error.innerHTML='No puede dejar campos sin completar.';
    return false;
  }

  if (clave!=confirmacion) {
    error.style.display='block';
    error.innerHTML='Las claves no coinciden.';
    return false;
  }
  return true;
}



function verificarComentario() {
  var titulo=document.getElementById('titulo').value;
  var comentario=document.getElementById('comentario').value;
  var error=document.getElementById('oculto');
  if (titulo=="" || comentario=="" ) {
    error.style.display='block';
    error.innerHTML='No puede dejar campos sin completar.';
    return false;
  }
  return true;
}


