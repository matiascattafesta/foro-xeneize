{include file="header.tpl" subtitulo="Comentarios" titulo="Foro Xeneixe" seccion="index"}

{foreach from=$coment item="com"}
	<div class="cuadroComentario">
		<div class="cuadroComentDatos">
			<div> De: {$com.nombre|escape:html} </div>    
			<div>{$com.fecha}</div>
			<div> 
				{if ($com.existeFoto==1)}
					<img src="foto/{$com.id_usuario}-{$com.nombre}.jpg">
				{else}
				  	{$com.mail|gravatar}
				{/if}
			</div>   
		</div>
		<div class="cuadroComentComent">
			<div class="titulo">
				<b>	{$com.titulo|escape:html} </b>
				{if ($com.nom==$com.nombre|lower)}
					<a href="borrarComentario.php?id={$com.id_comentario}" class="borrar">X</a>
				{/if}				
			</div>
			<div> {$com.comentario|escape:html|youtube|emoticon} </div>
		</div>
	</div>
{/foreach}

{include file="footer.tpl" seccion="index"}