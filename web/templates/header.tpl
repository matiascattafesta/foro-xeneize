<html>
	<head>
		<title>
			{$titulo}
		</title>
		<link rel="StyleSheet" href="styles/hojadeestilos.css" type="text/css">
		<script type="text/javascript" src="js/js.js"> </script>
	</head>

	<body>
		<div id="cabecera"> 	
			<h1 class="tituloprincipal">FORO XENEIZE</h1>
		</div>
		<img class="escudo" src="image/escudo.png" >
		{if $smarty.session.usuario==""}
			{if $seccion!='login'}
				<a class="log" href="login.php">Login</a>
			{/if}
		{else}
			<div>
				<a class="log" href="logout.php">Logout</a>
			</div>
			<div style="position:absolute;right:25px;z-index:3;">
				<a class="log" href="editarDatos.php">Editar Datos</a>
			</div>
		{/if}

		<div id="cabecera">
			<h2>{$subtitulo}</h2>
		</div>
		{if $smarty.session.usuario==""}
			{if $seccion!='registro'}
				<a class="reg" href="registro.php">Registrarse</a>
			{/if}
		{/if}

