<?php
// function smarty_function_gravatar( $email, $s = 80, $d = 'mm', $r = 'g', $img = true, $atts = array() ) {
//     $url = 'http://www.gravatar.com/avatar/';
//     $url .= md5( strtolower( trim( $email ) ) );
//     $url .= "?s=$s&d=$d&r=$r";
//     if ( $img ) {
//         $url = '<img src="' . $url . '"';
//         foreach ( $atts as $key => $val )
//             $url .= ' ' . $key . '="' . $val . '"';
//         $url .= ' />';
//     }
//     return $url;
// }


# http://www.marvinmarcelo.com/smarty-gravatar-plugin/ 
/** 
 * Gravatar 
 * 
 * @link http://www.marvinmarcelo.com 
 * 
 * @param array $params 
 * @param Smarty $smarty 
 */ 
function smarty_modifier_gravatar($email='') 
{ 
  /** 
   * constant $gravatar_host 
   */ 
  $gravatar_host = "http://www.gravatar.com/avatar/"; 
  
  /** 
   * @link http://en.gravatar.com/site/implement/url 
   */ 
  $hash = strtolower(md5(trim($email))); 
  $src = $gravatar_host . $hash . ".jpg?"; 
    
   $size = 60; 
   $src .= "s={$size}"; 

   $default = "identicon"; 
   $src .= "d={$default}"; 

   $rating = "G"; 
   $src .= "r={$rating}"; 
    
   $extras = ""; 
    
  return "<img src=\"$src\" />"; 
} 

