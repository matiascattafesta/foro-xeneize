<?php
/**
 * Table Definition for comentario
 */
require_once 'DB/DataObject.php';

class DataObjects_Comentario extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'comentario';                      // table name
    public $id_comentario;                   // int(11)  not_null primary_key auto_increment
    public $titulo;                          // string(45)  not_null
    public $comentario;                      // string(1000)  not_null
    public $fecha;                           // datetime(19)  not_null binary
    public $id_usuario;                      // int(11)  not_null multiple_key

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Comentario',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
