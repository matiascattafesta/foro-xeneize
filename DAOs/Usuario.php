<?php
/**
 * Table Definition for usuario
 */
require_once 'DB/DataObject.php';

class DataObjects_Usuario extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'usuario';                         // table name
    public $id_usuario;                      // int(11)  not_null primary_key auto_increment
    public $nombre;                          // string(45)  not_null unique_key
    public $password;                        // string(45)  not_null
    public $mail;                            // string(45)  not_null
    public $foto;                            // blob(65535)  blob binary
    public $existe_foto;                     // int(1)  not_null unsigned

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Usuario',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
